# [CNI Plugins](https://github.com/containernetworking/plugins/) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-cni-plugins.asc https://packaging.gitlab.io/cni-plugins/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/cni-plugins cni-plugins main" | sudo tee /etc/apt/sources.list.d/morph027-cni-plugins.list
```
